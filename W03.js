//  This file has our application code. 
//  Complete this code so all QUnit tests pass. 
//  We have defined an App object that performs math functions.
//  A JS object is a set of key-value pairs, more like a dictionary in other languages.
var App = {
  add: function(...argv) {
    /* This is a thing in Javascript I guess.
     * I can't call arguments.reduce() directly, so I had to convert it 
     * to an array. Reduce is essentially a for loop. arguments is LIKE
     * an array, but it is NOT an array. Which means it doesn't have 
     * any array properties... Why Javascript? Why can't we have nice things?
     */
    return argv.reduce((sum, element) => sum += element);
  },
  square: function(base) {
    if(typeof base === 'number' && isFinite(base))
      return (base * base);
    throw Error('The given argument is not a number');
  },
  max: function () {
    let max = -Infinity;
    for (let i = 0; i < arguments.length; i++) {
      if (arguments[i] > max) {
        max = arguments[i];
      }
    }
    return max;
  },
  isOdd: function (number) {
    if (typeof number !== 'number') {
      throw Error('The given argument is not a number');
    }
    return number % 2 !== 0;
  },
  sortObj: function (array) {
    array.sort(function (a, b) {
      const date1 = new Date(a.timestamp).getTime();
      const date2 = new Date(b.timestamp).getTime();

      if (date1 < date2) {
        return -1;
      } else if (date1 === date2) {
        return 0;
      } else {
        return 1;
      }
    });
  },
  pow: function (base, power) {
    let output = 1;
    for (let i = 0; i < power; i++) {
      output *= base;
    }
    return output;
  },
  sum: function () {
    let output = 0;
    for (let i = 0; i < arguments.length; i++) {
      output += arguments[i];
    }
    return output;
  }
};